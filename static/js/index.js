let webhookNames = [];   //names of all webhooks we have access to
let webhookData = [];    //data for all webhooks we have access to (note: URLs are not included)
let characterNames = []; //names of all characters we have access to
let characterData = [];  //data for all characters we have access to (note: avatars are not included)
let currentUser;         //username of the current user

$(document).ready(function () {

    //get the current user (this has to be a server call because the browser doesn't have access to the username)
    $.getJSON("/api/whoami", function (data) {
        currentUser = data;
        console.log("Current user: " + currentUser.name);

        //show the character admin button if the user has access to modify any characters
        if (currentUser.characterAdmin) {
            console.log("User has access to modify characters");
            $("#characterAdminButton").show();
        }
    });

    //fetch webhook data on page load
    $.getJSON("/api/webhooks", function (data) {

        //save this data for later use
        webhookData = data;
        webhookNames = data.map(webhook => webhook.name);

        //clear the list
        $("#webhooks").empty();

        //add "select a webhook" option
        $("#webhooks").append("<option value=''>Select a webhook</option>");

        //sort the webhooknames alphabetically
        webhookNames.sort(function (a, b) {
            if (a < b) return -1;
            if (a > b) return 1;
            return 0;
        });

        //populate the list
        $.each(webhookNames, function (index, webhook) {
            $("#webhooks").append("<option value='" + webhook + "'>" + webhook + "</option>");
        });

        console.log(data.length + " webhooks loaded");
    });

    //fetch character data on page load
    $.getJSON("/api/characters", function (data) {

        //save this data for later use
        characterData = data;
        characterNames = data.map(character => character.name);

        //sort the character names alphabetically
        characterNames.sort(function (a, b) {
            if (a < b) return -1;
            if (a > b) return 1;
            return 0;
        });

        //sort the character data by name
        characterData.sort(function (a, b) {
            if (a.name < b.name) return -1;
            if (a.name > b.name) return 1;
            return 0;
        });

        //populate the list
        populateCharacterList();

        console.log(characterNames.length + " characters loaded");
    });

    //populate the character list when any of the webhook or filter controls are changed
    $("#webhooks, #In_Setting, #In_Game, #Living, #Deity, #Major_Character, #Minor_Character, #Generic_Character").change(function () {
        populateCharacterList();
    });

    //character admin button handler
    $("#characterAdminButton").click(function () {
        window.location.href = "/charAdmin.html";
    });

    //send button handler
    $("#send").click(function () {
        const webhook = $("#webhooks").val();
        const character = $("#characters").val();
        const displayName = $("#displayName").val() || character;
        const message = $("#message").val();
        const thread = $("#thread").val() || 0;

        const messageBody = { webhook: webhook, character: character, message: message, thread: thread, displayName: displayName };

        //pass the data to the server, which will send it to the webhook
        $.ajax("/api/send", {
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(messageBody)
        }).done(function (res) {
            // console.log(res);
            $("#result").html("Success!");

            //clear the message box
            $("#message").val("");
        }).fail(function (err) {
            console.log(err);
            $("#result").html("Error: " + err.responseText);
        });
    });
});

function populateCharacterList() {
    console.log("populating character list");

    //save the current selection
    const selectedCharacter = $("#characters").val();

    $("#characters").empty();

    //add "select a character" option
    $("#characters").append("<option value=''>Select a character</option>");

    //populate the list
    const currentWebhook = webhookData.find(webhook => webhook.name === $("#webhooks").val());
    $.each(characterData, function (index, character) {
        
        //In Setting filter: skip the charcter only if all of the following are true
        // - the "in setting" checkbox is checked
        // - the webhook does not have the "ALL" setting
        // - the webhook does not share a setting with the character
        if ($("#In_Setting").prop('checked')) {
            if (currentWebhook && (currentWebhook.games) && (!currentWebhook.settings.includes("ALL"))) {
                let validSettingFound = false;
                if (character.settings) {
                    for (let setting of character.settings) {
                        if (currentWebhook?.settings) {
                            if (currentWebhook.settings.includes(setting)) {
                                validSettingFound = true;
                                break;
                            }
                        }
                    }
                }
                if (!validSettingFound) return;
            }
        }

        //In Game filter: skip the charcter only if all of the following are true
        // - the "in game" checkbox is checked
        // - the webhook does not have the "ALL" game
        // - the webhook does not share a game with the character
        if ($("#In_Game").prop('checked')) {
            if (currentWebhook && (currentWebhook.games) && (!currentWebhook.games.includes("ALL"))) {
                let validGameFound = false;
                if (character.games) {
                    for (let game of character.games) {
                        if (currentWebhook?.games) {
                            if (currentWebhook.games.includes(game)) {
                                validGameFound = true;
                                break;
                            }
                        }
                    }
                }
                if (!validGameFound) return;
            }
        }

        //if the "Living" filter is set and the character has the dead tag, skip the character
        if ($("#Living").prop('checked')) {
            if(character.tags && character.tags.includes("Dead")) { 
                return;
            }
        }

        //if the "Deity" filter is set and the character is not a deity, skip the character
        if ($("#Deity").prop('checked')) {
            if(!character.tags || !character.tags.includes("Deity")) {
                return;
            }
        }

        //if the "Major Character" filter is set and the character is not a major character, skip the character
        if ($("#Major_Character").prop('checked')) {
            if(!character.tags || !character.tags.includes("Major")) {
                return;
            }
        }

        //if the "Minor Character" filter is set and the character is not a minor character, skip the character
        if ($("#Minor_Character").prop('checked')) {
            if(!character.tags || !character.tags.includes("Minor")) {
                return;
            }
        }

        //if the "Generic Character" filter is set and the character is not a generic character, skip the character
        if ($("#Generic_Character").prop('checked')) {
            if(!character.tags || !character.tags.includes("Generic")) {
                return;
            }
        }

        //add the character to the list
        $("#characters").append("<option value='" + character.name + "'>" + character.name + "</option>");
    });

    //if the previous selection is still valid, select it again
    if ($("#characters option[value=\"" + selectedCharacter + "\"]").length > 0) {
        $("#characters").val(selectedCharacter);
    }
}

