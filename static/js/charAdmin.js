let characterNames = [];          //names of all characters we have access to
let characterData = [];           //data for all characters we have access to 
let knownSettings = [];           //list of settings that characters can reside in
let knownGames = [];              //list of games that characters can be part of
let knownTags = [];               //list of tags that can be used on characters
let creatingNewCharacter = false; //whether we are creating a new character or editing an existing one
let currentUser;                  //username of the current user

$(document).ready(function () {

    //hide the edit character form 
    $("#editCharacterForm").hide();

    //get the current user (this has to be a server call because the browser doesn't have access to the username)
    $.getJSON("/api/whoami", function (data) {
        currentUser = data;
        console.log("Current user: " + currentUser.name);
    });

    //fetch character data on page load
    loadCharacterData();

    //fetch known settings on page load
    $.getJSON("/api/settings", function (data) {
        knownSettings = data;
        console.log(knownSettings.length + " settings loaded");

        //use this list to populate options on the setting select
        $.each(knownSettings, function (index, tag) {
            $("#editCharacterSettings").append("<option value='" + tag + "'>" + tag + "</option>");
        });
    });

    //fetch known games on page load
    $.getJSON("/api/games", function (data) {
        knownGames = data;
        console.log(knownGames.length + " games loaded");

        //use this list to populate options on the game select
        $.each(knownGames, function (index, game) {
            $("#editCharacterGames").append("<option value='" + game + "'>" + game + "</option>");
        });
    });

    //fetch known tags on page load
    $.getJSON("/api/tags", function (data) { 
        knownTags = data;
        console.log(knownTags.length + " tags loaded");

        //use this list to populate options on the tag select
        $.each(knownTags, function (index, tag) {
            $("#editCharacterTags").append("<option value='" + tag + "'>" + tag + "</option>");
        });
    });

    //repopulate the character table when any of the filters change
    $("#charSearch, #settingFilter, #gameFilter, #tagFilter").on('change keyup paste', function () {
        populateCharacterTable();
    });
});

//fetch character data from the server
function loadCharacterData() {

    $.getJSON("/api/characters", function (data) {

        //save the data globally
        characterData = data;
        characterNames = data.map(character => character.name);

        //sort the character names alphabetically
        characterNames.sort(function (a, b) {
            if (a < b) return -1;
            if (a > b) return 1;
            return 0;
        });

        //sort the character data by name
        characterData.sort(function (a, b) {
            if (a.name < b.name) return -1;
            if (a.name > b.name) return 1;
            return 0;
        });

        //populate the table
        populateCharacterTable();

        console.log(characterNames.length + " characters loaded");
    });
}

//populate the character table
function populateCharacterTable() {

    //clear the table
    $("#characterTable").empty();

    //add the table header
    $("#characterTable").append("<tr><th>Name</th><th>Avatar</th><th>Settings</th><th>Games</th><th>Tags</th><th>Owner</th><th>Actions</th></tr>");

    //filter the characters based on the search text
    let searchText = $("#charSearch").val().toLowerCase();

    //populate the table
    $.each(characterData, function (index, character) {

            //filter the characters based on the search text
            if (character.name.toLowerCase().includes(searchText) || 
                 character.owner.toLowerCase().includes(searchText) ||
                (character.settings?.toString()?.toLowerCase() || "").includes(searchText) || 
                (character.games?.toString()?.toLowerCase() || "").includes(searchText) || 
                (character.tags?.toString()?.toLowerCase() || "").includes(searchText))
            {
               //add the character row 
               $("#characterTable").append("<tr><td>" + character.name + "</td>" + 
               "<td><img src='" + character.url + "' width='64' height='64'></td>" + 
               "<td>" + (character.settings || "") + "</td>" + 
               "<td>" + (character.games || "") + "</td>" + 
               "<td>" + (character.tags || "" ) + "</td>" + 
               "<td>" + character.owner + "</td>" + 
               "<td><button class='btn btn-primary' onclick='openEditForm(" + index + ")'>Edit</button> <button class='btn btn-danger' onclick='deleteCharacter(" + index + ")'>Delete</button></td>" + 
               "</tr>");
            }
        });
}

//opens the edit character form for the selected character
function openEditForm(characterIndex) {

    //find the character
    let character = characterData[characterIndex];

    //throw an error if the character doesn't exist
    if (!character) {
        alert("Character not found");
        return; 
    }

    //populate the form
    $("#editCharacterName").val(character.name);
    $("#editCharacterOwner").val(character.owner);
    $("#editCharacterAvatar").val(character.url);
    $("#editCharacterSettings").val(character.settings);
    $("#editCharacterGames").val(character.games);
    $("#editCharacterTags").val(character.tags);

    //show the edit character form and hide the character table
    $("#editCharacterForm").show();
    $("#characterTable").hide();
}

//opens the edit character form for a new character
function addCharacterForm() {

    creatingNewCharacter = true; //we are creating a new character

    //clear the form
    $("#editCharacterName").val("");
    $("#editCharacterOwner").val("");
    $("#editCharacterAvatar").val("");
    $("#editCharacterSettings").val("");
    $("#editCharacterGames").val("");
    $("#editCharacterTags").val("");
    
    //since this is a new character, the user should be allowed to edit the name
    $("#editCharacterName").prop("readonly", false);

    //since this is a new character, the current user should be the owner
    $("#editCharacterOwner").val(currentUser.name);

    //show the edit character form and hide the character table
    $("#editCharacterForm").show();
    $("#characterTable").hide();
}

//goes back to the index page
function backToMain() {
    window.location.href = "/";
}

//deletes the selected character, prompting the user to confirm
function deleteCharacter(characterIndex) {

    //find the character
    let character = characterData[characterIndex];

    //throw an error if the character doesn't exist
    if (!character) {
        alert("Character not found");
        return;
    }

    //confirm the deletion
    if (!confirm("Are you sure you want to delete " + character.name + "?")) {
        return;
    }

    //delete the character
    $.ajax({
        url: "/api/characters/" + character.name,
        type: "DELETE",
        success: function (data) {
            console.log("Character deleted");
            //refresh the character table
            loadCharacterData();
        },
        error: function (data) {
            alert("Error deleting character");
            alert(data.responseText);
        }
    });
}

//either creates or updates a character
function saveCharacter() {

    //get the character data from the form
    let character = {
        name: $("#editCharacterName").val(),
        owner: $("#editCharacterOwner").val(),
        url: $("#editCharacterAvatar").val(),
        settings: $("#editCharacterSettings").val(),
        games: $("#editCharacterGames").val(),
        tags: $("#editCharacterTags").val()
    };
    
    if(creatingNewCharacter) {
        //create the character
        $.ajax({
            url: "/api/characters",
            contentType: "application/json",
            type: "POST",
            data: JSON.stringify(character),
            success: function (data) {
                console.log("Character created");

                //refresh the character table
                loadCharacterData();

                //go back to the character table
                $("#editCharacterForm").hide();
                $("#characterTable").show();

                //make sure the name is readonly again (it was temporarily disabled since this was a new character)
                $("#editCharacterName").prop("readonly", true);
            },
            error: function (data) {
                console.log(data);
                alert("Error creating character");
                alert(data.responseText);
            }
        });
    } else {
        //send the updated character data to the server
        $.ajax({
            url: "/api/characters/" + character.name,
            contentType: "application/json",
            type: "PUT",
            data: JSON.stringify(character),
            success: function (data) {
                console.log(data);
                $("#editCharacterForm").hide();
                $("#characterTable").show();
                loadCharacterData();
            },
            error: function (data) {
                console.log(data);
                alert("Error updating character");
                alert(data.responseText);
            }
        });
    }
}

//cancel the edit character form
function cancelEditCharacter() {
    $("#editCharacterForm").hide();
    $("#characterTable").show();

    //make sure the name is readonly again (it might have been temporarily disabled to allow editing if this was a new character)
    $("#editCharacterName").prop("readonly", true);
}

