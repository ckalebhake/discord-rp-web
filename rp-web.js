//main script for Discord-RP-Web

//dependencies
const express = require('express');              //express
const app = express();                           //express web server instance
const basicAuth = require('express-basic-auth'); //basic authentication for web server
const fs = require('fs');                        //file system
const path = require('path');                    //for platform-specific file paths
const axios = require('axios');                  //rest library

//server settings
const port = 30005; //port to run server on

//constants
const appDirectory = __dirname;                               //directory of this script
const secretDirectory = path.resolve(appDirectory, 'secret'); //directory of secret files
const configLocation = path.resolve(secretDirectory,'RP Config.json'); //location of RP Config file

//TODO: make these dynamic instead of hard coded
const knownSettings = ["Prima"];
const knownGames = ["OttR"];
const knownTags = ["Major","Minor","Deity","Dead","Generic"];

//RP data
let rpConfig; //stores the webhooks and characters in memory

//fetch authorized users from secret file
authorizedUsers = JSON.parse(fs.readFileSync(path.resolve(secretDirectory,'authorizedUsers.json'), 'utf8'));

//configure basic authentication
app.use(basicAuth({
    users: authorizedUsers.users,
    challenge: true,
    realm: 'Discord-RP-Web'
})); 

//interpret requests as JSON
app.use(express.json());

//serve static files
app.use(express.static(path.resolve(appDirectory, 'static')));

//listen for requests
app.listen(port, () => {
    console.log(`Server listening on port ${port}`);
});


/////////////////////////////////////////////////////////////////////
// API
/////////////////////////////////////////////////////////////////////

//provides info about the authenticated user
app.get('/api/whoami', (req, res) => {
    res.send(JSON.stringify({
        name: req.auth.user,
        webhookAdmin: hasWebhookAdminAccess(req.auth.user),
        characterAdmin: hasCharacterAdminAccess(req.auth.user)
    }));
});

//provides a list of all webhooks that the user is authorized to use
app.get('/api/webhooks', function(req, res) {
    //filter webhooks to only those that the user is authorized to use
    const authorizedWebhooks = rpConfig.webhooks.filter(webhook => canUseWebhook(req.auth.user, webhook.name));

    //make copies of each webhook that excludes the url
    const sanitizedWebhooks = authorizedWebhooks.map(webhook => {
        const sanitizedWebhook = {};
        for (const key in webhook) {
            if (key !== 'url') {
                sanitizedWebhook[key] = webhook[key];
            }
        }
        return sanitizedWebhook;
    });

    //send the list of webhooks
    res.send(sanitizedWebhooks);
});

//provides a list of all characters that the user is authorized to use
app.get('/api/characters', function(req, res) {
    //filter characters to only those that the user is authorized to use
    const authorizedCharacters = rpConfig.characters.filter(character => canUseCharacter(req.auth.user, character.name));

    //make copies of each character so the original can't be modified
    const sanitizedCharacters = authorizedCharacters.map(character => {
        const sanitizedCharacter = {};
        for (const key in character) {
            // if (key !== 'url') {
                sanitizedCharacter[key] = character[key];
            // }
        }
        return sanitizedCharacter;
    });

    //send the list of characters
    res.send(sanitizedCharacters);
});

//sends a message to a webhook
app.post('/api/send', function(req, res) {
    console.log(req.body);
    
    //get the webhook name and message
    const webhookName = req.body.webhook;
    const thread = req.body.thread;
    const characterName = req.body.character;
    const displayName = req.body.displayName || characterName;
    const messageBody = req.body.message;

    //find the webhook
    const webhook = rpConfig.webhooks.find(webhook => webhook.name === webhookName);

    //find the character
    const character = rpConfig.characters.find(character => character.name === characterName);

    //if the webhook doesn't exist, return an error
    if (!webhook) {
        res.status(400).send('Webhook not found');
        return;
    }

    //if the character doesn't exist, return an error
    if (!character) {
        res.status(400).send('Character not found');
        return;
    }

    //if the message is empty, return an error
    if (!messageBody) {
        res.status(400).send('Message cannot be empty');
        return;
    }

    //if the sending user isn't authorized, return an error
    if (!canUseWebhook(req.auth.user, webhookName)) {
        res.status(403).send('You are not authorized to send messages from that webhook');
        return;
    }

    //send the message to the webhook
    console.log(req.auth.user + " is sending a message to " + webhook.name + (thread ? "(t:"+thread+")" : "") +" as " + character.name);
    
    sendRP(webhook, character, messageBody, thread, displayName).then(function (rpResult) {
        res.status(200).send();
    }).catch(function (rpError) {
        console.log(rpError);
        res.status(500).send("Error while talking to discord:\n\t" + rpError.status + ": " + rpError.statusText);
    });
});

//provides the list of known settings
app.get('/api/settings', function(req, res) {
    res.send(knownSettings);
});

//provides the list of known games
app.get('/api/games', function(req, res) {
    res.send(knownGames);
});

//provides the list of tags that can be used on characters
app.get('/api/tags', function(req, res) {
    res.send(knownTags);
});

//updates an existing character
app.put('/api/characters/:characterName', function(req, res) {
    //get the character name and new data
    const characterName = req.params.characterName;
    const newCharacterData = req.body;

    //find the character
    const character = rpConfig.characters.find(character => character.name === characterName);

    //if the character doesn't exist, return an error
    if (!character) {
        res.status(400).send('Character not found');
        return;
    }

    //the name in the new data must match the name of the character passed in the URL
    if (newCharacterData.name !== characterName) {
        res.status(400).send('Character name in URL must match name in data');
        return;
    }

    //if the requesting user isn't authorized, return an error
    if (!canModifyCharacter(req.auth.user, characterName)) {
        res.status(403).send('You are not authorized to modify this character');
        return;
    }

    //log the change
    console.log("User " + req.auth.user + " changed character " + characterName + ":");
    console.log(newCharacterData)

    //update the character, except for the name and owner fields
    for (const key in newCharacterData) {
        if (key !== 'name' && key !== 'owner') {    
            character[key] = newCharacterData[key];
        }
    }

    //save the config
    saveRpConfig();

    //send a success response
    res.status(200).send();
});

//creates a new character
app.post('/api/characters', function(req, res) {
    console.log(req.body);

    //get the new character data
    const newCharacterData = req.body;

    //if the character name is empty, return an error
    if (!newCharacterData.name) {
        res.status(400).send('Character name cannot be empty');
        return;
    }

    //if the character name is already in use, return an error
    if (rpConfig.characters.find(character => character.name === newCharacterData.name)) {
        res.status(400).send('Character name is already in use');
        return;
    }

    //log the change
    console.log("User " + req.auth.user + " created character " + newCharacterData.name + ":");
    console.log(newCharacterData)

    //add the character to the config
    rpConfig.characters.push(newCharacterData);

    //save the config
    saveRpConfig();

    //send a success response
    res.status(200).send();
});

//deletes a character
app.delete('/api/characters/:characterName', function(req, res) {
    //get the character name
    const characterName = req.params.characterName;

    //find the character
    const character = rpConfig.characters.find(character => character.name === characterName);

    //if the character doesn't exist, return an error
    if (!character) {
        res.status(400).send('Character not found');
        return;
    }

    //if the requesting user isn't authorized, return an error
    if (!canModifyCharacter(req.auth.user, characterName)) {
        res.status(403).send('You are not authorized to modify this character');
        return;
    }

    //log the change
    console.log("User " + req.auth.user + " deleted character " + characterName);

    //remove the character from the config
    rpConfig.characters = rpConfig.characters.filter(character => character.name !== characterName);

    //save the config
    saveRpConfig();

    //send a success response
    res.status(200).send();
});

/////////////////////////////////////////////////////////////////////
// Permissions
/////////////////////////////////////////////////////////////////////

//checks if a user is authorized to use a webhook
const canUseWebhook = function(user, webhookName)
{
    //if the user does not exist, return false
    if (!authorizedUsers.users[user])
    {
        return false;
    }

    //if the user is authorized to use all webhooks, return true
    if (authorizedUsers.authorization[user].webhooks.includes("ALL"))
    {
        return true;
    }

    //if the user is authorized to use owned webhooks and they own the webhook, return true
    if (authorizedUsers.authorization[user].webhooks.includes("OWNED") && 
        rpConfig.webhooks.find(webhook => webhook.name === webhookName && webhook.owner === user))
    {
        return true;
    }

    //if the user is authorized to use the webhook, return true
    return authorizedUsers.authorization[user].webhooks.includes(webhookName)
}

//checks if a user is authorized to use a character
const canUseCharacter = function(user, characterName)
{
    //if the user does not exist, return false
    if (!authorizedUsers.users[user])
    {
        return false;
    }

    //if the user is authorized to use all characters, return true
    if (authorizedUsers.authorization[user].characters.includes("ALL"))
    {
        return true;
    }

    //if the user is authorized to use owned characters and they own the character, return true
    if (authorizedUsers.authorization[user].characters.includes("OWNED") &&
        rpConfig.characters.find(character => character.name === characterName && character.owner === user))
    {
        return true;
    }
}

//checks if a user is authorized to modify a character
const canModifyCharacter = function(user, characterName)
{
    //if the user does not exist, return false
    if (!authorizedUsers.users[user])
    {
        return false;
    }

    //if the user is authorized to modify all characters, return true
    if (authorizedUsers.authorization[user].characterAdmin.includes("ALL"))
    {
        return true;
    }

    //if the user is authorized to modify owned characters and they own the character, return true
    if (authorizedUsers.authorization[user].characterAdmin.includes("OWNED") &&
        rpConfig.characters.find(character => character.name === characterName && character.owner === user))
    {
        return true;
    }

    //otherwise, the user is authorized to modify the character if it is in their characterAdmin list
    return authorizedUsers.authorization[user].characterAdmin.includes(characterName)
}

//returns true if the requesting user should have access to the webhook admin panel
const hasWebhookAdminAccess = function(user) {
    if(authorizedUsers.authorization[user].webhookAdmin) {
        return true
    } else {
        return false;
    }
};

//returns true if the requesting user should have access to the character admin panel
const hasCharacterAdminAccess = function(user) {
    if(authorizedUsers.authorization[user].characterAdmin) {
        return true
    } else {
        return false;
    }
};

/////////////////////////////////////////////////////////////////////
// file handling
/////////////////////////////////////////////////////////////////////

const loadRpConfig = function () 
{ 
    rpConfig = JSON.parse(fs.readFileSync(configLocation)); 
    console.log((rpConfig.webhooks?.length || 0) + " Webhooks, " + (rpConfig.characters?.length || 0) + " Characters loaded.");
}

const saveRpConfig = function () { 
    fs.writeFileSync(configLocation, JSON.stringify(rpConfig)); 
    loadRpConfig(); //immediately reload the config we just saved to make sure data in memory stays in sync with data on disc
}

/////////////////////////////////////////////////////////////////////
// work with webhooks
/////////////////////////////////////////////////////////////////////

//sends an RP chat message
const sendRP = async function(webhook, character, message, thread, displayName)
{
    const url=webhook.url + (thread? ("?thread_id=" + thread) : "");
    // console.log(url)
    return axios.post(url, {
        "username": displayName || character.name,
        "avatar_url": character.url,
        "content": message
    }).then(function (res) {
        console.log(res);
        return res.data;
    }) 
    .catch(function (err) { 
        //log and return errors
        if (err.response)
        {
            console.error(err.response?.status + ": " + err.response?.statusText);
            console.error(err.response?.data); 
        }
        else
        {
            console.error(err.message);
        }
        return err;
    }); 
}

/////////////////////////////////////////////////////////////////////
// startup steps
/////////////////////////////////////////////////////////////////////
loadRpConfig(); //load the config file

//throw warnings if the authorized users file contains invalid webhook or character names
for (const user in authorizedUsers.users)
{
    for (const webhookName of authorizedUsers.authorization[user].webhooks)
    {
        //don't warn about "ALL" or "OWNED"
        if (webhookName === "ALL" || webhookName === "OWNED")
        {
            continue;
        }

        if (!rpConfig.webhooks.find(webhook => webhook.name === webhookName))
        {
            console.warn("[WARN] User " + user + " is authorized to use webhook " + webhookName + ", but that webhook does not exist.");
        }
    }

    for (const characterName of authorizedUsers.authorization[user].characters)
    {
        //don't warn about "ALL" or "OWNED"
        if (characterName === "ALL" || characterName === "OWNED")
        {
            continue;
        }

        if (!rpConfig.characters.find(character => character.name === characterName))
        {
            console.warn("[WARN] User " + user + " is authorized to use character " + characterName + ", but that character does not exist.");
        }
    }
}

//throw warnings if any characters in the config file have unknown tags
for (const character of rpConfig.characters)
{
    //skip if the character has no tags
    if (!character.tags)
    {
        continue;
    }

    for (const tag of character.tags)
    {
        if (!knownTags.includes(tag))
        {
            console.warn("[WARN] Character " + character.name + " has unknown tag " + tag + ".");
        }
    }
}

//throw warnings if any characters in the config file have no owner
for (const character of rpConfig.characters)
{
    if (!character.owner)
    {
        console.warn("[WARN] Character " + character.name + " has no owner.");
    }
}